package de.fh_kiel.c3boto.chat_reader.keywords;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.pircbotx.User;
import org.pircbotx.hooks.events.PrivateMessageEvent;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import java.util.UUID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({PrivateMessageEvent.class})
public class PrivateKeywordsTest {
    static User vUser;
    static UUID vUUID = UUID.randomUUID();
    final static long id = 100;
    final static  String vorname = "Max";
    final static String nachname = "";
    final static String strasse = "";
    final static  String hausnummer = "";
    final static String postleitzahl = "0815";
    final static String ort = "Entenhausen";

    final static String expectedMessage = "Hi " + vorname + " nice to meet you!";
    final static PrivateMessageEvent vTestEvent = mock(PrivateMessageEvent.class);

    @Before
    public void setUp() throws Exception {
        vUser = mock(User.class);
        when(vUser.getUserId()).thenReturn(vUUID);
        when(vTestEvent.getId()).thenReturn(id);
        when(vTestEvent.getUser()).thenReturn(vUser);
    }

    @Test
    public void testRegister(){
        String vMessage = "?register name=" + vorname + " last_name=" + nachname + " strasse=" + strasse +
                " hausnummer=" + hausnummer + " postleitzahl=" + postleitzahl + " ort=" + ort;
        when(vTestEvent.getMessage()).thenReturn(vMessage);

        assertThat(vTestEvent.getUser().getUserId()).isEqualTo(vUUID);
        Keyword.lookUp(vTestEvent);
        verify(vTestEvent, times(1)).respondPrivateMessage(expectedMessage);
    }

    @Test
    public void testShowInfo(){
        String vMessage = "?showinfo";
        when(vTestEvent.getMessage()).thenReturn(vMessage);

        Keyword.lookUp(vTestEvent);
        verify(vTestEvent, times(0)).respondPrivateMessage("");
    }

    @Test
    public void testAddInterestHelp(){

    }
}