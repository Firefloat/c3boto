package de.fh_kiel.c3boto.chat_reader.parser;

import de.fh_kiel.c3boto.chat_reader.keywords.StringInformation;
import org.junit.Test;
import java.util.Map;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class InformationParserTest {
    private static final String message = "?register name=Michael last_name=Variola strasse=Hauptstrasse " +
            "hausnummer=19 postleitzahl=24112 ort=Kiel";

    @Test
    public void extractKeyValuePairs() {
        Map<String, String> userInformation = InformationParser.extractKeyValuePairs(message);

        assertThat(userInformation.get(StringInformation.VORNAME)).isEqualTo("Michael");
        assertThat(userInformation.get(StringInformation.NACHNAME)).isEqualTo("Variola");
        assertThat(userInformation.get(StringInformation.STRASSE)).isEqualTo("Hauptstrasse");
        assertThat(userInformation.get(StringInformation.HAUSNUMMER)).isEqualTo("19");
        assertThat(userInformation.get(StringInformation.POSTLEITZAHL)).isEqualTo("24112");
        assertThat(userInformation.get(StringInformation.ORT)).isEqualTo("Kiel");
    }
}