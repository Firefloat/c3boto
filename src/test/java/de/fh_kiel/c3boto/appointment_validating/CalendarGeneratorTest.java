package de.fh_kiel.c3boto.appointment_validating;

import de.fh_kiel.c3boto.user.user_data.Termin;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNull;


public class CalendarGeneratorTest {

    private static Termin termin;
    private static List<Termin> appointmentList;

    public static void assertReaders(BufferedReader expected,
                                     BufferedReader actual) throws IOException {
        String line;
        while ((line = expected.readLine()) != null) {
            assertEquals(line, actual.readLine());
        }

        assertNull("Actual had more lines then the expected.", actual.readLine());
        assertNull("Expected had more lines then the actual.", expected.readLine());
    }

    @Before
    public void setUp() throws Exception {
        appointmentList = new ArrayList<>();
        termin = new Termin()
                .setStartDate("01-01-2000-19:00")
                .setEndDate("02-02-2001-19:00")
                .setInfo("Test")
                .setBezeichnung("Test");
        appointmentList.add(termin);
    }

    @Test
    public void testCalendarGenerator(){
        CalendarGenerator.generate(appointmentList, "test_output.txt");

        BufferedReader expected = null;
        BufferedReader actual = null;
        try {
             expected = new BufferedReader(new FileReader("outputFolder/test_compare.txt"));
             actual = new BufferedReader(new FileReader("outputFolder/test_output.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            assertReaders(expected, actual);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}