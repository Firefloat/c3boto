package de.fh_kiel.c3boto.user.user_data;

import org.junit.Before;
import org.junit.Test;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

public class UserTest {
    private static User user;
    private static Termin termin;
    private static Address address;

    @Before
    public void setUp() {
        String userId = "abcdef";
        termin = new Termin("05-05-2017-15:30", "06-06-2017-15:30", 0, "Testing", "Testing");
        address = new Address("Hauptstrasse", "17", "24114", "Kiel");
        user = new User("Max", "Mustermann", address, userId).addTermin(termin);
    }

    @Test
    public void testSetup(){
        assertThat(user.getTermin(0)).isNotEqualTo(null);
        assertThat(user.getTermin(0).getInfo()).isEqualTo("Testing");
        assertThat(user.getAdresse()).isNotEqualTo(null);
        assertThat(user.getId()).isNotEmpty();
    }

    @Test
    public void testUserIdCheck(){
        assertThat(user.checkIfUserIdIs("abcdef")).isTrue();
    }
}