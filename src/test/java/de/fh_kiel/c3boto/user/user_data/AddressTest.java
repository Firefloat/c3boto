package de.fh_kiel.c3boto.user.user_data;

import org.junit.Before;
import org.junit.Test;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

public class AddressTest {
    private static Address address;
    private final static String strasse = "Hauptstrasse";
    private final static String hausnummer = "15";
    private final static String postleitzahl = "24114";
    private final static String ort = "Kiel";

    @Before
    public void setUp() throws Exception {
        address = new Address();
    }

    @Test
    public void testBasicClass(){
        assertThat(address.getStrasse()).isEqualTo("");
        assertThat(address.getPostleitzahl()).isEqualTo("");
        assertThat(address.getHausnummer()).isEqualTo("");
        assertThat(address.getOrt()).isEqualTo("");
    }

    @Test
    public void testSetterChaining(){
        address.setStrasse(strasse).setHausnummer(hausnummer).setPostleitzahl(postleitzahl).setOrt(ort);
        assertThat(address.getStrasse()).isEqualTo(strasse);
        assertThat(address.getPostleitzahl()).isEqualTo(postleitzahl);
        assertThat(address.getHausnummer()).isEqualTo(hausnummer);
        assertThat(address.getOrt()).isEqualTo(ort);
    }
}