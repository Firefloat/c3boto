package de.fh_kiel.c3boto.user.user_data;

import org.junit.Before;
import org.junit.Test;
import static org.assertj.core.api.AssertionsForClassTypes.catchThrowable;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

public class TerminTest {
    private static Termin termin;
    private static Termin terminToCopy;
    private final static String startDate = "05-06-2019-15:45";
    private final static String endDate = "06-07-2019-23:59";
    private final static String bezeichnung = "Testing";

    @Before
    public void setUp(){
        termin = new Termin();
        terminToCopy = new Termin();
        terminToCopy.setBezeichnung(bezeichnung);
        terminToCopy.setStartDate(startDate);
        terminToCopy.setEndDate(endDate);
    }

    @Test
    public void testStringToDate(){
        termin.setStartDate(startDate);
        termin.setEndDate(endDate);

        assertThat(termin.getStartDate()).hasDayOfMonth(5);
        assertThat(termin.getStartDate()).hasMonth(6);
        assertThat(termin.getStartDate()).hasYear(2019);
        assertThat(termin.getStartDate()).hasHourOfDay(15);
        assertThat(termin.getStartDate()).hasMinute(45);

        assertThat(termin.getEndDate()).hasDayOfMonth(6);
        assertThat(termin.getEndDate()).hasMonth(7);
        assertThat(termin.getEndDate()).hasYear(2019);
        assertThat(termin.getEndDate()).hasHourOfDay(23);
        assertThat(termin.getEndDate()).hasMinute(59);
    }

    @Test
    public void endDateBeforeStartDateException(){
        Termin exceptionTermin = new Termin().setEndDate(startDate);

        Throwable thrown = catchThrowable(() -> {exceptionTermin.setStartDate(endDate); });

        assertThat(thrown)
                .isInstanceOf(StartDateAfterEndDateException.class)
                .hasNoCause()
                .hasStackTraceContaining("Starting date can't be greater than end date!");
    }

    @Test
    public void testCopyConstructor(){
        final String bezeichnungChange = "Changed";
        final String newStartDate = "02-09-2014-15:33";
        final String newEndDate = "03-09-2015-12:00";

        Termin terminCopy = new Termin(terminToCopy);
        terminToCopy.setStartDate(newStartDate);
        terminToCopy.setEndDate(newEndDate);
        terminToCopy.setBezeichnung(bezeichnungChange);

        assertThat(terminCopy.getBezeichnung()).isEqualTo(bezeichnung);
        assertThat(terminCopy.getStartDate().toString()).isEqualTo("Wed Jun 05 15:45:00 CEST 2019");
        assertThat(terminCopy.getEndDate().toString()).isEqualTo("Sat Jul 06 23:59:00 CEST 2019");
        assertThat(terminCopy.getId()).isEqualTo(terminToCopy.getId());
    }
}