package de.fh_kiel.c3boto.user;

import de.fh_kiel.c3boto.user.user_data.User;
import org.junit.Before;
import org.junit.Test;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

public class UserManagerTest {
    private static UserManager userManager;
    private static User user1;
    private static User user2;
    private static UserManager userManagerComparer;

    @Before
    public void setUp() throws Exception {
        userManager = UserManager.getInstance();
        userManagerComparer = UserManager.getInstance();
        user1 = new User();
        user2 = new User();
    }

    @Test
    public void testSingleton(){
        assertThat(userManager).isEqualTo(userManagerComparer);
    }

    @Test
    public void addAndRemoveUser(){
        userManager.addUserToPool(user1);
        userManager.addUserToPool(user2);

        assertThat(userManager.getUserFromPool(0)).isEqualTo(user1);
        assertThat(userManager.getUserFromPool(1)).isEqualTo(user2);

        userManager.removeUserFromPool(0);

        assertThat(userManager.getUserFromPool(0)).isEqualTo(user2);

        userManager.addUserToPool(user1);
        userManager.removeUserFromPool(user2);

        assertThat(userManager.getUserFromPool(0)).isEqualTo(user1);

    }
}