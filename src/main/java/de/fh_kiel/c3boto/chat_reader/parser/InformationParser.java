package de.fh_kiel.c3boto.chat_reader.parser;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InformationParser {

    private InformationParser(){}

    public static Map<String, String> extractKeyValuePairs(String message){
        Map<String, String> userInformation = new HashMap<>();

        String pattern = "\\b([^\\s]+)=([^\\s]+)\\b";
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(message);

        while (m.find()) {
            userInformation.put(m.group(1), m.group(2));
        }
        return userInformation;
    }
}
