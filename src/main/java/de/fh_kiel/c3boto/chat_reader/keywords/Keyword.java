package de.fh_kiel.c3boto.chat_reader.keywords;

import de.fh_kiel.c3boto.appointment_validating.CalendarGenerator;
import de.fh_kiel.c3boto.chat_reader.parser.InformationParser;
import de.fh_kiel.c3boto.user.UserManager;
import de.fh_kiel.c3boto.user.user_data.StartDateAfterEndDateException;
import de.fh_kiel.c3boto.user.user_data.Termin;
import de.fh_kiel.c3boto.user.user_data.User;
import org.pircbotx.hooks.events.PrivateMessageEvent;
import org.pircbotx.hooks.types.GenericMessageEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Implementation of the different keywords that exist. The Keyword Class is a "Wrapper" for multiple overloaded
 * lookUp-Methods. Depending of the kind of message (eg. PrivateMessageEvent) the right method gets executed.
 *
 * Those Methods execute the Enum Operations (implemented through lambda-expressions) depending on the first part of
 * the String, which has to be the name of the Enum-Operation.
 *
 * For example: the PrivateKeywords.REGISTER.respond(event) - Method gets executed if the first string is ?register
 *
 * @author Michael Variola
 */
public class Keyword {
    private Keyword(){}

    /**
     *
     * @param privateMessageEvent
     */
    public static void lookUp(PrivateMessageEvent privateMessageEvent){
        PrivateKeywords.valueOf(extractKeyword(privateMessageEvent.getMessage())).respond(privateMessageEvent);
    }

    /**
     *
     * @param genericMessageEvent
     */
    public static void lookUp(GenericMessageEvent genericMessageEvent){
        PublicKeywords.valueOf(extractKeyword(genericMessageEvent.getMessage())).respond(genericMessageEvent);
    }

    /**
     *
     * @param message
     * @return
     */
    private static String extractKeyword(String message){
        return message.split(" ")[0].substring(message.indexOf(StringInformation.KEYWORD_START_SIGN) + 1).toUpperCase();
    }
}

interface PrivateKeyworInterpreter {
    void respond(PrivateMessageEvent event);
}
enum PrivateKeywords implements PrivateKeyworInterpreter {

    SHOWINFO((PrivateMessageEvent event) -> {
        for (User user : UserManager.getInstance().getUserPool()){
            if (user.checkIfUserIdIs(event.getUser().getUserId().toString())){
                event.respondPrivateMessage(user.print());
            }
        }
    }),

    REGISTER((PrivateMessageEvent event) -> {
            Map<String, String> userInformation = InformationParser.extractKeyValuePairs(event.getMessage());
            UserManager.getInstance().addUserToPool(new User(userInformation)
                    .setId(event.getUser().getUserId().toString()));
            event.respondPrivateMessage("Hi " + userInformation.get(StringInformation.VORNAME) + " nice to meet you!");
        }
    ),

    ADD_INTEREST_HELP((PrivateMessageEvent event) -> event.respondPrivateMessage(StringInformation.ADD_INTEREST_HELP)),

    ADD_INTEREST((PrivateMessageEvent event) -> {
        Map<String, String> userInformation = InformationParser.extractKeyValuePairs(event.getMessage());
        for (User user : UserManager.getInstance().getUserPool()){
            if (user.checkIfUserIdIs(event.getUser().getUserId().toString())){
                user.addKompetenz(userInformation.get(StringInformation.INTEREST));
            }
        }
    }),

    SHOW_INTERESTS((PrivateMessageEvent event) -> {
        for (User user : UserManager.getInstance().getUserPool()){
            if (user.checkIfUserIdIs(event.getUser().getUserId().toString())){
                event.respondPrivateMessage(user.printKompetenzen());
            }
        }
    }),

    HELP((PrivateMessageEvent event) -> event.respondPrivateMessage(StringInformation.HELPTEXT)),

    APPOINTMENT_HELP((PrivateMessageEvent event) -> event.respondPrivateMessage(StringInformation.TERMIN_HELP)),

    ADD_APPOINTMENT((PrivateMessageEvent event) -> {
        Map<String, String> appointmentInformation = InformationParser.extractKeyValuePairs(event.getMessage());
        for (User user : UserManager.getInstance().getUserPool()){
            if (user.checkIfUserIdIs(event.getUser().getUserId().toString())){
                try{
                    user.addTermin(new Termin(appointmentInformation));
                } catch (StartDateAfterEndDateException e){
                    event.respondPrivateMessage(e.getMessage());
                }
            }
        }
    }),

    CALENDAR_INFO((PrivateMessageEvent event) -> event.respondPrivateMessage(StringInformation.CALENDAR_INFO)),

    CREATE_CALENDAR((PrivateMessageEvent event) -> {
        Map<String, String> fileInformation = InformationParser.extractKeyValuePairs(event.getMessage());
        for (User user : UserManager.getInstance().getUserPool()){
            if (user.checkIfUserIdIs(event.getUser().getUserId().toString())){
                List<Termin> appointmentList = new ArrayList<>();
                for (Termin termin : user.getTermine()){
                    appointmentList.add(termin);
                }
                CalendarGenerator.generate(appointmentList, fileInformation.get("file_name"));
                event.respondPrivateMessage("All Done!");
            }
        }
    }),

    SHOW_APPOINTMENTS((PrivateMessageEvent event) -> {
        for (User user : UserManager.getInstance().getUserPool()){
            if (user.checkIfUserIdIs(event.getUser().getUserId().toString())){
                for (Termin termin : user.getTermine()){
                    event.respondPrivateMessage(termin.print());
                }
            }
        }
    });

    private final PrivateKeyworInterpreter interpreter;

    PrivateKeywords(final PrivateKeyworInterpreter keywordInterpreter){
        this.interpreter = keywordInterpreter;
    }

    @Override
    public void respond(PrivateMessageEvent event) {
        this.interpreter.respond(event);
    }
}

interface GenericKeywordInterpreter {
    void respond(GenericMessageEvent event);
}
enum PublicKeywords implements GenericKeywordInterpreter {
    HELP((GenericMessageEvent event) -> event.respond(StringInformation.HELPTEXT)),

    SHOWUSER((GenericMessageEvent event) -> {
        for (User user: UserManager.getInstance().getUserPool()){
            event.respond(user.getVorname() + " " + user.getNachname());
        }
        if (UserManager.getInstance().getUserPool().isEmpty()){
            event.respond(StringInformation.NO_USER_REGISTERED);
        }
    }),

    HELLOWORLD((GenericMessageEvent event) -> event.respond(StringInformation.HELLO_WORLD)),

    TESTID((GenericMessageEvent event) -> event.respond(event.getUser().getUserId().toString())),

    REGISTER_INFO((GenericMessageEvent event) -> event.respondPrivateMessage(StringInformation.REGISTER_HELP));

    private final GenericKeywordInterpreter interpreter;

    PublicKeywords(final GenericKeywordInterpreter keywordInterpreter){
        this.interpreter = keywordInterpreter;
    }

    @Override
    public void respond(GenericMessageEvent event) {
        this.interpreter.respond(event);
    }
}