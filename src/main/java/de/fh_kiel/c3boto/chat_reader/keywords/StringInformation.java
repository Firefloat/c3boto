package de.fh_kiel.c3boto.chat_reader.keywords;

/**
 * Data container for all the strings needed for the user interaction. Class has no other functionality
 * @author Michael Variola
 */
public class StringInformation {
    private StringInformation(){}

    public static final String BOT_NAME = "C3BOTO";
    public static final String SERVER_ADDRESS = "192.168.0.100";
    public static final String CHANNEL_TO_CONNECT = "#seg";
    public static final String VORNAME = "name";
    public static final String NACHNAME = "last_name";
    public static final String STRASSE = "strasse";
    public static final String HAUSNUMMER = "hausnummer";
    public static final String POSTLEITZAHL = "postleitzahl";
    public static final String ORT = "ort";
    public static final String HELLO_WORLD = "Hello World!";
    public static final String KEYWORD_START_SIGN = "?";
    public static final String INTEREST = "interest";
    public static final String DATE_FORMAT = "dd-MM-yyyy-HH:mm";
    public static final String APPOINTMENT_START_DATE = "start_date";
    public static final String APPOINTMENT_END_DATE = "end_date";
    public static final String APPOINTMENT_INFO = "info";
    public static final String APPOINTMENT_DESCRIPTION = "description";
    public static final String CALENDAR_INFO = "To generate a .csv file for google calendar, simply type " +
            "?create_calendar file_name=output_file_name.csv";
    public static final String PRIVATE_HELPTEXT =
            " The following keywords can be used for user interaction directly chatting with C3BOTO " +
            java.util.Arrays.asList(PrivateKeywords.values()).toString();
    public static final String ADD_INTEREST_HELP = "To add an new interest, just write " + KEYWORD_START_SIGN +
            "add_interest " + INTEREST + "=your_interest";
    public static final String NO_USER_REGISTERED = "No user is currently registered. Use the " + KEYWORD_START_SIGN +
            "register_info function to get information how to register";
    public static final String HELPTEXT = "Keywords must be begin with a " + KEYWORD_START_SIGN +
            " The following keywords can be used for user interaction in the MainChannel " +
            java.util.Arrays.asList(PublicKeywords.values()).toString() + PRIVATE_HELPTEXT;
    public static final String REGISTER_HELP = "Hi there, if you want to register respond to this " +
            "private Message with ?register " + VORNAME + "=your_name " + NACHNAME + "=your_last_name " + STRASSE +
            "=your_street_name " + HAUSNUMMER + "=your_street_number " + POSTLEITZAHL + "=your_zip_code " + ORT +
            "=your_municipality";
    public static final String TERMIN_HELP = "To add an appointment to your User information write ?add_appointment " +
            APPOINTMENT_START_DATE + "=" + DATE_FORMAT + " " + APPOINTMENT_END_DATE + "="+DATE_FORMAT + " " +
            APPOINTMENT_INFO + "=" + "appointment_information " + APPOINTMENT_DESCRIPTION + "=" + "appointment_description";
}
