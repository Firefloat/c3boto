package de.fh_kiel.c3boto.user;

import de.fh_kiel.c3boto.user.user_data.User;
import java.util.ArrayList;
import java.util.List;

/**
 * UserManager hold all the Users registered to the bot. It is implemented as singleton-pattern to make sure only one
 * instance exists at a time.
 * @author Michael Variola
 */
public class UserManager {
    private List<User> userPool;
    private static UserManager instance;
    private List<org.pircbotx.User> botPool;

    private UserManager(){
        this.userPool = new ArrayList<>();
        this.botPool = new ArrayList<>();
    }

    public static UserManager getInstance(){
        if (instance == null){
            instance = new UserManager();
        }
        return instance;
    }

    public void addUserToPool(User user){
        this.userPool.add(user);
    }

    public User getUserFromPool(int index){
        return this.userPool.get(index);
    }

    public List<User> getUserPool(){
        return this.userPool;
    }

    public void removeUserFromPool(int index){
        this.userPool.remove(index);
    }

    public void removeUserFromPool(User user){
        this.userPool.remove(user);
    }

    public void notifyUser(){
        for (User user: userPool){
            // TODO Notify User when appointment is canceled
        }
    }

    public List<org.pircbotx.User> getBotPool() {
        return botPool;
    }

    public void setBotPool(List<org.pircbotx.User> botPool) {
        this.botPool = botPool;
    }
}
