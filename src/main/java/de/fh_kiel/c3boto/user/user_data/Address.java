package de.fh_kiel.c3boto.user.user_data;

/**
 * Represents an address of an user or of a termin.
 * @author Michael Variola
 */
public class Address {
    private String strasse;
    private String hausnummer;
    private String postleitzahl;
    private String ort;

    public Address(){
        this.setStrasse("");
        this.setHausnummer("");
        this.setPostleitzahl("");
        this.setOrt("");
    }

    public Address(Address other){
        this.setStrasse(other.getStrasse());
        this.setHausnummer(other.getHausnummer());
        this.setPostleitzahl(other.getPostleitzahl());
        this.setOrt(other.getOrt());
    }

    public Address(String strasse, String hausnummer, String postleitzahl, String ort){
        this.setStrasse(strasse);
        this.setHausnummer(hausnummer);
        this.setPostleitzahl(postleitzahl);
        this.setOrt(ort);
    }

    public Address setStrasse(String strasse) {
        this.strasse = strasse;
        return this;
    }

    public Address setHausnummer(String hausnummer) {
        this.hausnummer = hausnummer;
        return this;
    }

    public Address setPostleitzahl(String postleitzahl) {
        this.postleitzahl = postleitzahl;
        return this;
    }

    public Address setOrt(String ort) {
        this.ort = ort;
        return this;
    }

    public String getStrasse() {
        return strasse;
    }

    public String getHausnummer() {
        return hausnummer;
    }

    public String getPostleitzahl() {
        return postleitzahl;
    }

    public String getOrt() {
        return ort;
    }

    public String print(){
        return this.getStrasse() + " " + this.getHausnummer() + " " +  this.getPostleitzahl() + " " + this.getOrt();
    }
}
