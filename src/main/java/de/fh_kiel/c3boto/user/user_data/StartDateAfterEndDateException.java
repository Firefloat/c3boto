package de.fh_kiel.c3boto.user.user_data;

public class StartDateAfterEndDateException extends RuntimeException {

    @Override
    public String toString() {
        return "Starting date can't be greater than end date!";
    }
}
