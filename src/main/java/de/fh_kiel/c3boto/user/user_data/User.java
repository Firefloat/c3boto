package de.fh_kiel.c3boto.user.user_data;

import de.fh_kiel.c3boto.chat_reader.keywords.StringInformation;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Class represents a basic User. Class User is just a data container
 * */
public class User {
    private String vorname;
    private String nachname;
    private Address adresse;
    private List<Termin> termine;
    private String id;
    private List<String> kompetenzen;

    public User(){
        this("", "", new Address(), "");
    }

    public User(String vorname, String nachname, Address adresse, String id){
        this.setId(id);
        this.setVorname(vorname);
        this.setNachname(nachname);
        this.setAdresse(adresse);
        this.termine = new ArrayList<>();
        this.kompetenzen = new ArrayList<>();
    }

    /**
     * Constructor to create an User from a Map.
     * @param initializerMap Map with key-value-pairs.
     * @see StringInformation for the key to find different information
     */
    public User(Map<String, String> initializerMap){
        this(initializerMap.get(StringInformation.VORNAME), initializerMap.get(StringInformation.NACHNAME),
                new Address()
                .setStrasse(initializerMap.get(StringInformation.STRASSE))
                .setHausnummer(initializerMap.get(StringInformation.HAUSNUMMER))
                .setPostleitzahl(initializerMap.get(StringInformation.POSTLEITZAHL))
                .setOrt(initializerMap.get(StringInformation.ORT)), "");
    }

    public User setVorname(String vorname) {
        this.vorname = vorname;
        return this;
    }

    public User setNachname(String nachname) {
        this.nachname = nachname;
        return this;
    }

    public User setAdresse(Address adresse) {
        this.adresse = adresse;
        return this;
    }

    public User addTermin(Termin termin) {
        this.termine.add(termin);
        return this;
    }

    public User addKompetenz(String kompetenz){
        this.kompetenzen.add(kompetenz);
        return this;
    }

    public User setId(String id) {

        this.id = id;
        return this;
    }

    public List<Termin> getTermine(){
        return this.termine;
    }

    public Termin getTermin(int index){
        return this.termine.get(index);
    }

    public String getVorname() {
        return vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public Address getAdresse() {
        return adresse;
    }

    public String getId() {
        return id;
    }

    public String getKompetenz(int index){
        return this.kompetenzen.get(index);
    }

    public String print(){
        return this.getVorname() + " " + this.getNachname() + " ID: " + this.getId() + " Adresse: "
                + this.getAdresse().print();
    }

    public String printKompetenzen(){
        StringBuilder builder = new StringBuilder();
        for (String komp : this.kompetenzen){
            builder.append(komp + " ");
        }
        return builder.toString();
    }

    public boolean checkIfUserIdIs(String userId){
        return this.id.equals(userId);
    }
}
