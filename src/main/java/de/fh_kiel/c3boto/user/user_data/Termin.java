package de.fh_kiel.c3boto.user.user_data;

import de.fh_kiel.c3boto.chat_reader.keywords.StringInformation;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

/**
 * Termin-Class to store Termin Information.
 */
public class Termin {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(StringInformation.DATE_FORMAT); // Date format
    private Date startDate;
    private Date endDate;
    private String id;
    private int groupId;
    private String info;
    private String bezeichnung;

    public Termin(){
        this.setId();
    }

    /**
     * Constructor to create a Termin from a Map.
     * @param appointmentInformation Map with key-value-pairs.
     * @see StringInformation for the key declaration to find different information
     */
    public Termin(Map<String, String> appointmentInformation){
        this.setStartDate(appointmentInformation.get(StringInformation.APPOINTMENT_START_DATE));
        this.setEndDate(appointmentInformation.get(StringInformation.APPOINTMENT_END_DATE));
        this.setId();
        this.setInfo(appointmentInformation.get(StringInformation.APPOINTMENT_INFO));
        this.setBezeichnung(appointmentInformation.get(StringInformation.APPOINTMENT_DESCRIPTION));
    }

    public Termin(final Termin other){
        this.id = other.getId(); // TODO Does copied Termin need own id?
        this.setStartDate(new Date(other.getStartDate().getTime()));
        this.setEndDate(new Date(other.getEndDate().getTime()));
        this.setGroupId(other.getGroupId());
        this.setInfo(other.getInfo());
        this.setBezeichnung(other.getBezeichnung());
    }

    public Termin(Date startDate, Date endDate, int groupId, String info, String bezeichnung){
        this.setId();
        this.setStartDate(startDate);
        this.setEndDate(endDate);
        this.setGroupId(groupId);
        this.setInfo(info);
        this.setBezeichnung(bezeichnung);
    }

    public Termin(String startDate, String endDate, int groupId, String info, String bezeichnung){
        this.setId();
        this.setStartDate(startDate);
        this.setEndDate(endDate);
        this.setGroupId(groupId);
        this.setInfo(info);
        this.setBezeichnung(bezeichnung);
    }

    /**
     * @param endDate Date in string format. Gets parsed into Date object.
     * @return Instance of itself for chaining
     */
    public Termin setEndDate(String endDate){
        try {
            this.endDate = DATE_FORMAT.parse(endDate);
            assertEndDateGreaterThanStartDate();

        } catch (ParseException e) {
            System.out.println(e.toString());
        }
        return this;
    }

    /**
     * @param startDate Date in string format. Gets parsed into Date object
     * @return Instance of itself for chaining
     */
    public Termin setStartDate(String startDate){
        try {
            this.startDate = DATE_FORMAT.parse(startDate);
            assertEndDateGreaterThanStartDate();
        } catch (ParseException e) {
            System.out.println(e.toString());
        }
        return this;
    }

    public Termin setStartDate(Date startDate) {
        this.startDate = startDate;
        assertEndDateGreaterThanStartDate();
        return this;
    }

    public Termin setEndDate(Date endDate) {
        this.endDate = endDate;
        assertEndDateGreaterThanStartDate();
        return this;
    }

    public Termin setGroupId(int groupId) {
        this.groupId = groupId;
        return this;
    }

    private void setId() {
        this.id = UUID.randomUUID().toString();
    }

    public Termin setInfo(String info) {
        this.info = info;
        return this;
    }

    public Termin setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
        return this;
    }

    public String getId() {
        return id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public int getGroupId() {
        return groupId;
    }

    public String getInfo() {
        return info;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    /**
     * Method checks if start date is located before end date
     * @throws StartDateAfterEndDateException RuntimeException extension to signal error
     * @see de.fh_kiel.c3boto.user.user_data.StartDateAfterEndDateException
     */
    private void assertEndDateGreaterThanStartDate(){
        if (this.startDate != null && this.endDate != null && this.startDate.after(endDate)){
            throw new StartDateAfterEndDateException();
        }
    }

    public String print(){
        return "AppointmentID: " + this.getId() + ", from " + this.getStartDate().toString() + " to " +
                this.getEndDate().toString() + ", Information: " + this.getInfo() + ", Description: " + this.getBezeichnung();
    }
}