package de.fh_kiel.c3boto;

import de.fh_kiel.c3boto.chat_reader.keywords.Keyword;
import de.fh_kiel.c3boto.chat_reader.keywords.StringInformation;
import de.fh_kiel.c3boto.user.UserManager;
import org.pircbotx.Channel;
import org.pircbotx.Configuration;
import org.pircbotx.PircBotX;
import org.pircbotx.User;
import org.pircbotx.dcc.ReceiveChat;
import org.pircbotx.dcc.SendChat;
import org.pircbotx.exception.IrcException;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.*;
import org.pircbotx.hooks.types.GenericMessageEvent;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.List;


public class C3Listener extends ListenerAdapter {

    @Override
    public void onIncomingChatRequest(IncomingChatRequestEvent event) throws Exception {

        ReceiveChat chat = event.accept();

        String line;

        while ((line = chat.readLine()) != null) {
            String response = "You said " + line;
            chat.sendLine(response);
            System.out.println("Sent line: " + response);
        }
    }

    @Override
    public void onMessage(MessageEvent event) throws Exception {
        //Only trigger when the user says ?chat
        if (!event.getMessage().startsWith("?chat"))
            return;

        //Attempt to get a DCC chat with the user
        SendChat chat = event.getUser().send().dccChat();
        UserManager.getInstance().setBotPool(event.getBot().getUserBot().getChannels().first().getUsers().asList());

        for (User user : UserManager.getInstance().getBotPool()){
            event.respond(user.toString());
        }
        
        //Were now connected to the user
        //In this example do the same thing as the IncommingChatRequest example
        String line;
        while ((line = chat.readLine()) != null) {
            String response = "You said " + line;
            chat.sendLine(response);
            System.out.println("Sent line: " + response);
        }
    }

    @Override
    public void onGenericMessage(GenericMessageEvent event) {
        if (event.getMessage().startsWith(StringInformation.KEYWORD_START_SIGN)){
            Keyword.lookUp(event);
        }
        logMessageToFile(event);
    }

    @Override
    public void onPrivateMessage(PrivateMessageEvent event) throws Exception {
        super.onPrivateMessage(event);
        if (event.getMessage().startsWith(StringInformation.KEYWORD_START_SIGN)){
            Keyword.lookUp(event);
        }
    }

    private void logMessageToFile(GenericMessageEvent event){
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter("chat_log.txt", true));
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            bw.write(event.getUser().getNick() + " - " + sdf.format(event.getTimestamp()) + " : "+ event.getMessage());
            bw.newLine();
            bw.close();
        } catch (IOException e) {
            System.out.println(e.toString());
        }
    }

    public static void main(String[] args) throws Exception {
        //Configure what we want our bot to do
        Configuration configuration = new Configuration.Builder()
                .setName(StringInformation.BOT_NAME) //Set the nick of the bot.
                .addServer(StringInformation.SERVER_ADDRESS) //Join the local-server
                .addAutoJoinChannel(StringInformation.CHANNEL_TO_CONNECT)
                .addAutoJoinChannel("#BotChannel")//Join the right channel
                .addListener(new C3Listener()) //Add our listener that will be called on Events
                .buildConfiguration();

        //Create our bot with the configuration
        try (PircBotX bot = new PircBotX(configuration)) {
            //Connect to the server
            bot.startBot();
        } catch (IOException | IrcException e) {
            System.out.println(e.toString());
        }
    }
}

