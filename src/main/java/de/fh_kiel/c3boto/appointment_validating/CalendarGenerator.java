package de.fh_kiel.c3boto.appointment_validating;

import com.opencsv.CSVWriter;
import de.fh_kiel.c3boto.user.user_data.Termin;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

public class CalendarGenerator {
    private CalendarGenerator(){}
    private static final String OUTPUT_FOLDER = "outputFolder/";
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy"); // Date format
    private static final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm"); // Date format

    private static String filePath = OUTPUT_FOLDER;

    public static void generate(List<Termin> appointmentList, String outputFileName){

        File file = new File(filePath += outputFileName);
        try {
            // create FileWriter object with file as parameter
            FileWriter outputfile = new FileWriter(file);

            // create CSVWriter object filewriter object as parameter
            CSVWriter writer = new CSVWriter(outputfile);

            String[] header = {"Subject", "Start Date", "Start Time", "End Date", "End Time", "Description", "Location"};

            writer.writeNext(header);

            for (Termin termin : appointmentList){
                String[] data = {termin.getInfo(),
                        DATE_FORMAT.format(termin.getStartDate()),
                        TIME_FORMAT.format(termin.getStartDate()),
                        DATE_FORMAT.format(termin.getEndDate()),
                        TIME_FORMAT.format(termin.getEndDate()),
                        termin.getBezeichnung(),
                        ""};
                writer.writeNext(data);
            }

            // closing writer connection
            writer.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
