# Installation 

## Server

Als erstes muss Docker installiert werden. Siehe [hier](https://runnable.com/docker/install-docker-on-windows-10)

Die Installation erfolgt unter Mac OS auf die selbe Art.

* [Docker for Windows](https://hub.docker.com/editions/community/docker-ce-desktop-windows)
* [Docker for Mac](https://hub.docker.com/editions/community/docker-ce-desktop-mac)

**Es muss ein Account erstellt werden um Docker downloaden zu können**

Nachdem dieses erfolgreich installiert ist, kann der container mit dem Befehl:

        docker run --name inspircd -p 6667:6667 -v /path/to/your/config:/inspircd/conf/ inspircd/inspircd-docker

gepullt und auch gestartet werden. Dabei ist zu beachten, dass der **/path/to/your/config** auf den Ordner server_config des Projektes weist. Danach ist der server unter localhost:6667 erreichbar.

## Client

Als Client kann jeder IRC-Client verwendet werden. Für Windows empfielt sich der Client [HexChat](https://hexchat.github.io/downloads.html) für Mac OS kann [LimeChat](http://limechat.net/mac/) verwendet werden.

Um sich mit dem Server zu verbinden, muss als Serveradresse "localhost" und für den Port 6667 eingegeben werden. Danach sollte sich der Client problemlos verbinden.

Bevor der Chat funktioniert muss allerdings noch ein neuer Channel erstellt werden. Dies erfolgt durch den Befehl:

        /JOIN <channel_name>

Danach sollte das normale Chatten funktionieren

## Bot Starten

Der Code im Java Projekt ist soweit eingestellt, dass sich der Bot mit dem Server automatisch verbinden soll und auf die Anfrage **?helloworld** mit **Hello World!** antwortet.

Dazu muss nur ein Channel mit dem namen test erstellt werden:

    /JOIN test

Danach muss das die Main in der Klasse MyListener ausgeführt und kurz gewartet werden, bis sich der Bot mit dem Chat verbindet. Anschließend sollte der Bot auf die Usereingabe antworten.

## Befehlsliste

Unter [diesem Link](https://docs.inspircd.org/3/commands/) sind weitere Befehle für die Interaktion mit dem Server zu finden.

